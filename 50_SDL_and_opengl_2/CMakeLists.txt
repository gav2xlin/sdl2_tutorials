cmake_minimum_required(VERSION 3.5)

project(50_SDL_and_opengl_2 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(SDL2)
if (NOT SDL2_FOUND)
    include(FetchContent)

    FetchContent_Declare(
            SDL2
            GIT_REPOSITORY https://github.com/libsdl-org/SDL.git
            GIT_TAG release-2.28.2
            GIT_SHALLOW TRUE
            GIT_PROGRESS TRUE
    )
    FetchContent_MakeAvailable(SDL2)
endif()

find_package(OpenGL REQUIRED)

add_executable(50_SDL_and_opengl_2 main.cpp)
target_link_libraries(50_SDL_and_opengl_2 SDL2 ${OPENGL_LIBRARY})

install(TARGETS 50_SDL_and_opengl_2
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
