cmake_minimum_required(VERSION 3.5)

project(04_key_presses LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(SDL2)
if (NOT SDL2_FOUND)
    include(FetchContent)

    FetchContent_Declare(
            SDL2
            GIT_REPOSITORY https://github.com/libsdl-org/SDL.git
            GIT_TAG release-2.28.2
            GIT_SHALLOW TRUE
            GIT_PROGRESS TRUE
    )
    FetchContent_MakeAvailable(SDL2)
endif()

add_executable(04_key_presses main.cpp)
target_link_libraries(04_key_presses SDL2)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/bmp/
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/bmp/)

install(TARGETS 04_key_presses
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
