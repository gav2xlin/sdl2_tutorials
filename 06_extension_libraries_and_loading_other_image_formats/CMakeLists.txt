cmake_minimum_required(VERSION 3.5)

project(06_extension_libraries_and_loading_other_image_formats LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(SDL2)
if (NOT SDL2_FOUND)
    include(FetchContent)

    FetchContent_Declare(
            SDL2
            GIT_REPOSITORY https://github.com/libsdl-org/SDL.git
            GIT_TAG release-2.28.2
            GIT_SHALLOW TRUE
            GIT_PROGRESS TRUE
    )
    FetchContent_MakeAvailable(SDL2)
endif()

add_executable(06_extension_libraries_and_loading_other_image_formats main.cpp)
target_link_libraries(06_extension_libraries_and_loading_other_image_formats SDL2 SDL2_image)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/loaded.png
     DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

install(TARGETS 06_extension_libraries_and_loading_other_image_formats
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
